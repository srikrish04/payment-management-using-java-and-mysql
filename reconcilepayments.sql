
// reconciling payments to track payments based on orders.
use sasidharan;
ALTER TABLE payments  drop  foreign key payments_ibfk_1;
ALTER TABLE payments  drop primary key;
alter table payments add column (orderNumber int);
alter table payments add unique  key(customerNumber,checkNumber ,orderNumber);
alter table  payments add FOREIGN KEY (customerNumber) references customers(customerNumber);
alter table  payments add FOREIGN KEY (orderNumber) references orders(orderNumber);
update payments p, (select o.customerNumber,o.orderNumber,o.orderDate,sum(priceEach*quantityOrdered) as totalAmount from orders o inner join orderdetails od on od.orderNumber = o.orderNumber  group by o.orderNumber ) as val  set p.orderNumber = val.orderNumber where p.customerNumber = val.customerNumber  and p.amount = val.totalAmount;



//to drop my reconciling logic

delete from payments where   checkNumber = '<the checkNumber that is added>';

alter table payments drop  foreign key payments_ibfk_2;
alter table payments drop  foreign key payments_ibfk_1;
alter table payments drop index customerNumber;
alter table payments drop column orderNumber;

alter table payments add primary key(customerNumber,checkNumber);
alter table  payments add FOREIGN KEY (customerNumber) references customers(customerNumber);
