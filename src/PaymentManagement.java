import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class PaymentManagement {
	void reconcilePayments(Connection connection) {
		Statement statement = null;
		ResultSet resultSet= null;
		DatabaseMetaData databaseMetaData = null;
		try {
			statement = connection.createStatement();
			statement.execute("use sasidharan;");


			databaseMetaData = connection.getMetaData();
			resultSet = databaseMetaData.getSchemas(null, "sasidharan");
			resultSet = databaseMetaData.getColumns("sasidharan", null, "payments", "orderNumber");
			if(!resultSet.next())  {
				int removeConstraint = statement.executeUpdate("ALTER TABLE payments  drop  foreign key payments_ibfk_1;");
				int dropPrimaryKey = statement.executeUpdate("ALTER TABLE payments  drop primary key;");
				
				int addColumn = statement.executeUpdate("alter table payments add column (orderNumber int);");
				int addUniqueKey = statement.executeUpdate("alter table payments add unique  key(customerNumber,checkNumber ,orderNumber);");
				int addCustomerForeignKey = statement.executeUpdate("alter table  payments add FOREIGN KEY (customerNumber) references customers(customerNumber);");
				int addForeignKey = statement.executeUpdate("alter table  payments add FOREIGN KEY (orderNumber) references orders(orderNumber);");
				int updateAllPaymentRecords = statement.executeUpdate("update payments p, (select o.customerNumber,o.orderNumber,o.orderDate,sum(priceEach*quantityOrdered) as totalAmount from orders o inner join orderdetails od on od.orderNumber = o.orderNumber  group by o.orderNumber ) as val  set p.orderNumber = val.orderNumber where p.customerNumber = val.customerNumber  and p.amount = val.totalAmount;");

			}
			resultSet.close();
			statement.close();

		} catch (SQLException e) {
			System.out.println("error occured while reconciling the Payments table");
		}


	}

	boolean payOrder( Connection database, double amount, String cheque_number, ArrayList<Integer> orders ) {
		boolean status = false;
		Statement statement = null;
		ResultSet resultSet= null;
		reconcilePayments(database);

		try {
			statement = database.createStatement();
			statement.execute("use sasidharan;");
			Map<Integer, Double> perOrderPrice = new HashMap<Integer, Double>();
			List<Orders> allOrders = new ArrayList<Orders>();
			Set<Integer> customer = new HashSet<Integer>();
			for(Integer order:orders) {
				resultSet = statement.executeQuery("select o.orderNumber, sum(od.priceEach*quantityOrdered) as totalOrderCost, o.customerNumber  from orders o inner join orderdetails od on od.orderNumber = o.orderNumber where o.orderNumber = "+ order+" group by o.orderNumber;");
				while(resultSet.next()) {
					Orders ord = new Orders();

					perOrderPrice.put(Integer.parseInt(resultSet.getString("orderNumber")), Double.parseDouble(resultSet.getString("totalOrderCost")));
					ord.setCustomerNumber(Integer.parseInt(resultSet.getString("customerNumber")));
					ord.setOrderCost(Double.parseDouble(resultSet.getString("totalOrderCost")));
					ord.setOrderNumber(Integer.parseInt(resultSet.getString("orderNumber")));
					customer.add(Integer.parseInt(resultSet.getString("customerNumber")));
					
					
					allOrders.add(ord);
					
				}
				

			}
			
			if(customer.size()>1) {
				System.out.println("Customers can pay only for their own orders.");
				return false;
			}
			if(orders.size() == allOrders.size()) {
			double sumOfAllOrderAmounts = 0;
			for(Entry<Integer, Double> e:perOrderPrice.entrySet()) {
				sumOfAllOrderAmounts += e.getValue();
			}
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
			Date date = new Date();  
			String today = formatter.format(date);  			
			if(amount == sumOfAllOrderAmounts) {
				int rowsAffected =0 ;
				for(Orders or:allOrders) {
					rowsAffected += statement.executeUpdate("insert into payments(customerNumber,checkNumber,paymentDate,amount,orderNumber) values("+or.getCustomerNumber()+",'"+cheque_number+"','"+today+"',"+or.getOrderCost()+","+or.getOrderNumber()+");");
				}
				System.out.println("rowsAffected in Payments table:"+rowsAffected);

				status = true;
			}else {
				System.out.println("The given amount is not sufficient for all order payments");
			}
			
			}else {
				System.out.println("Please check orders list. The following orders are not found at the database: ");
				
				for(Integer order:orders) {
					
					if(!perOrderPrice.containsKey(order)) {
						System.out.print(" "+order);
					}
				}
			}
			resultSet.close();
			statement.close();

		}catch (SQLException e) {
			System.out.println("Error while inserting payments. ");
		}

		return status;
	}

	ArrayList<Integer> unpaidOrders( Connection database ){
		ArrayList<Integer> listOfUnpaidOrders = new ArrayList<Integer>();
		Statement statement = null;
		ResultSet resultSet= null;
		reconcilePayments(database);

		try {
			statement = database.createStatement();
			statement.execute("use sasidharan;");

			resultSet = statement.executeQuery("select o.orderNumber from orders o left join payments p on o.orderNumber = p.orderNumber where o.status not in ('Disputed','Cancelled') and p.orderNumber is null;");

			if(resultSet != null) {
				while (resultSet.next()) {
					listOfUnpaidOrders.add(Integer.parseInt(resultSet.getString("orderNumber")));
				}
			}
			
			if(listOfUnpaidOrders!=null && listOfUnpaidOrders.size()>0) {
				System.out.println("listOfUnpaidOrders count:"+listOfUnpaidOrders.size());
			}
			resultSet.close();
			statement.close();

		}catch (SQLException e) {
			System.out.println("Error while fetching unpaid orders");
		}

		return listOfUnpaidOrders;

	}
	ArrayList<String> unknownPayments( Connection database ){
		ArrayList<String> listOfUnknownPayments = new ArrayList<String>();
		Statement statement = null;
		ResultSet resultSet= null;
		reconcilePayments(database);

		try {
			statement = database.createStatement();
			statement.execute("use sasidharan;");

			resultSet = statement.executeQuery("select * from payments where orderNumber is null;");

			if(resultSet != null) {
				while (resultSet.next()) {
					listOfUnknownPayments.add(resultSet.getString("checkNumber"));
				}
			}
			
			if(listOfUnknownPayments!=null && listOfUnknownPayments.size()>0) {
				System.out.println("listOfUnknownPayments count:"+listOfUnknownPayments.size());
			}
			resultSet.close();
			statement.close();

		}catch (SQLException e) {
			System.out.println("Error while fetching unknown payments");
		}

		return listOfUnknownPayments;

	}
	
	
}
